import termcolor
from logic import *

trainers = ["Antonio", "Rodriho", "Mykola"]
teams = ["Milan", "Real", "Metalist"]

symbols = []

knowledge = And()

for trainer in trainers:
    for team in teams:
        symbols.append(Symbol(f"{trainer}{team}"))

for trainer in trainers:
    knowledge.add(Or(
        Symbol(f"{trainer}{teams[0]}"),
        Symbol(f"{trainer}{teams[1]}"),
        Symbol(f"{trainer}{teams[2]}")
    ))

for trainer in trainers:
    for team1 in teams:
        for team2 in teams:
            if team1 != team2:
                knowledge.add(
                    Implication(Symbol(f"{trainer}{team1}"), Not(Symbol(f"{trainer}{team2}")))
                )

for team in teams:
    for trainer1 in trainers:
        for trainer2 in trainers:
            if trainer1 != trainer2:
                knowledge.add(
                    Implication(Symbol(f"{trainer1}{team}"), Not(Symbol(f"{trainer2}{team}")))
                )

# It is known that nationality of all trainers do not corresponds to teams' nationality
knowledge.add(Not(Symbol("AntonioMilan")))
knowledge.add(Not(Symbol("RodrihoReal")))
knowledge.add(Not(Symbol("MykolaMetalist")))

# It is known that Metalist does not train with Antonio
knowledge.add(Not(Symbol("AntonioMetalist")))

# It is known that Real promised to not take Mykola as a trainer
knowledge.add(Not(Symbol("MykolaReal")))

for symbol in symbols:
    if model_check(knowledge, symbol):
        print(symbol)

